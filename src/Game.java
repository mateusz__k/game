import java.util.InputMismatchException;
import java.util.Scanner;

public class Game {

    public Game() {
    }

    public String printHint(String gameWord) {

        Scanner scanner = new Scanner(System.in);
        String input = scanner.next();
        StringBuilder stringBuilder = new StringBuilder();
        String output = "";
        if (gameWord.length() == input.length()) {
            char[] gameArray = gameWord.toCharArray();
            char[] inputArray = input.toCharArray();
            for (int i = 0; i < gameArray.length; i++) {
                if (gameArray[i] != inputArray[i]) {
                    stringBuilder.append("-");
                } else {
                    stringBuilder.append("X");
                }

                output = stringBuilder.toString();
            }
        } else {
            System.out.println("PLEASE ENTER A " + gameWord.length() + " LETTER WORD");
        }
        return output;
    }

    public String guessWord(String gameWord) {

        System.out.println("TAKE A GUESS!");

        Scanner scanner = new Scanner(System.in);
        String input = scanner.next();

        if (gameWord.equalsIgnoreCase(input)) {
            return "YOU WIN! " + gameWord.toUpperCase() + " IS A CORRECT WORD!";
        } else {
            return "WRONG ANSWER! KEEP TRYING";
        }
    }

    public void menu(String gameWord, Game game) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("<<<<<<<<<<<<>>>>>>>>>>>");
        System.out.println("WHAT DO YOU WANT TO DO?");
        System.out.println("1. HINT");
        System.out.println("2. GUESS");
        System.out.println("3. NEW WORD");
        System.out.println("4. EXIT");
        System.out.println("<<<<<<<<<<<<>>>>>>>>>>>");
        int choose = scanner.nextInt();

        switch (choose) {
            case 1:
                System.out.println("<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>");
                System.out.println("ENTER A " + gameWord.length() + " LETTER WORD TO GET A HINT:");
                System.out.println(game.printHint(gameWord));
                menu(gameWord, game);
            case 2:
                System.out.println(game.guessWord(gameWord));
                menu(gameWord, game);
            case 3:
                generateWord();
                menu(gameWord, game);
            case 4:
                System.out.println("BYE BYE!");
                break;
        }
    }

    public String generateWord() {
        Scanner scanner = new Scanner(System.in);
        Word word = new Word();
        int length = 0;

        System.out.println("");
        System.out.println("Enter word length: ");
        try {
            length = scanner.nextInt();
        } catch (InputMismatchException x) {
            System.out.println("PLEASE ENTER A NUMBER!");
        }
        String gameWord = word.getRandomWord(length);
        System.out.println("");
        System.out.println(length + " letter word generated!");
        System.out.println("");
        return gameWord;
    }
}
