public class Word {
    String word;

    public Word() {
        this.word = getRandomWord(0);
    }

    String getRandomWord(int length) {
        switch (length) {
            case 1 : return "x";
            case 2 : return "op";
            case 3 : return "pis";
            case 4 : return "ussr";
            case 5 : return "dunaj";
            case 6 : return "kajaak";
            case 7 : return "costams";
            case 8 : return "bleblebl";
            case 9 : return "dziewiec9";
            case 10 : return "dziesiec10";
        }
         return "PLEASE ENTER A NUMBER IN A 1-10 RANGE!";
    }
}
